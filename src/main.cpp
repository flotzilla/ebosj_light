#include <Arduino.h>
#include "NeoLight.h"
#include "ButtonHandler.h"
#include "AnalogButtonHandler.h"
#include "RGBHandler.h"

using namespace std;

#define LED_PIN 2
#define POWER_BUTN_PIN 3
#define BUTT1_PIN 4
#define BUTT2_PIN 5
#define BUTT3_PIN 6
#define POTENTIOMETER_PIN A0
#define NUMPIXELS 72

#define PIN_R A1
#define PIN_G A2
#define PIN_B A3

int initial_light_intensity = 10;

void noActionHandler()
{ /** no action **/
}
void pixelsUpdateState(); /** will be reimplemented **/
void analogUpdate();      /** will be reimplemented **/
void rgbPotetionmeterUpdate();

NeoLight pixels(NUMPIXELS, LED_PIN, NEO_RGB + NEO_KHZ800, &pixelsUpdateState);

void buttonOneOn()
{
  // Next mode
  Serial.println("button 1 clicked");
  pixels.Increment();
}

void buttonTwoOn()
{
  // Prev mode
  Serial.println("button 2 clicked");
  pixels.Reverse();
}

void buttonThreeOn()
{
  Serial.println("button 3 clicked");
  pixels.changeMode();
}

void powerButtonOnHandler()
{
  // turn off the lamp
  Serial.println("set br to 0");
  pixels.turnOff();
}

void powerButtonOffHandler()
{
  // turn on the lamp
  Serial.println("set initial color");
  pixels.turnOn();
}

int checkLowerThanZero(int x){
  if (x <= 0) {
    return 1;
  } else {
    return x;
  }
}

int checkGraterThen255(int x){
  if (x >= 255) {
    return 255;
  } else {
    return x;
  }
}

ButtonHandler butt1(BUTT1_PIN, 40, &buttonOneOn, &noActionHandler);
ButtonHandler butt2(BUTT2_PIN, 40, &buttonTwoOn, &noActionHandler);
ButtonHandler butt3(BUTT3_PIN, 40, &buttonThreeOn, &noActionHandler);
ButtonHandler powerButton(POWER_BUTN_PIN, 10, &powerButtonOnHandler, &powerButtonOffHandler);
AnalogButtonHandler analogReader(POTENTIOMETER_PIN, 50, &analogUpdate, &noActionHandler);

RGBHandler rgbReader(PIN_R, PIN_G, PIN_B, 50, &rgbPotetionmeterUpdate, &noActionHandler);

void setup()
{
  Serial.begin(115200);
  pinMode(BUTT1_PIN, INPUT);
  pinMode(BUTT2_PIN, INPUT);
  pinMode(BUTT3_PIN, INPUT);

  pixels.begin();
  pixels.setIntensity(initial_light_intensity);

  Serial.println("ready and stady");
}

void loop()
{
  unsigned long currentMillis = millis();
  powerButton.Update(currentMillis);

  if (powerButton.buttonState == HIGH)
  {
    butt1.Update(currentMillis);
    butt2.Update(currentMillis);
    butt3.Update(currentMillis);
    analogReader.Update(currentMillis);
    rgbReader.Update(currentMillis);

    pixels.Update();
  }
}

void pixelsUpdateState()
{
  unsigned long currentMillis = millis();

  powerButton.Update(currentMillis);

  butt1.Update(currentMillis);
  butt2.Update(currentMillis);
  butt3.Update(currentMillis);
  analogReader.Update(currentMillis);
  rgbReader.Update(currentMillis);
}

void analogUpdate()
{
  int brightness = round(((1024 - analogReader.buttonState) / 4) - 1);

  if (brightness <= 0)
  {
    brightness = 1;
  }

  if (brightness >= 255)
  {
    brightness = 255;
  }

  pixels.setIntensity(brightness);
}

void rgbPotetionmeterUpdate()
{

  int rColor = round(((rgbReader.buttonStateR) / 4) - 1);
  int gColor = round(((rgbReader.buttonStateG) / 4) - 1);
  int bColor = round(((rgbReader.buttonStateB) / 4) - 1);

  rColor = checkLowerThanZero(rColor);
  rColor = checkGraterThen255(rColor);
  gColor = checkLowerThanZero(gColor);
  gColor = checkGraterThen255(gColor);
  bColor = checkLowerThanZero(bColor);
  bColor = checkGraterThen255(bColor);

  pixels.setCustomColor(rColor, gColor, bColor);
  pixels.show();

  Serial.println(rColor);
  Serial.println(gColor);
  Serial.println(bColor);
  Serial.println(pixels.getCurrentLightMode());
  Serial.println("=========");
}