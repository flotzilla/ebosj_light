#include <Arduino.h>
#include "RGBHandler.h"

using namespace std;
    
RGBHandler::RGBHandler(int redPin, int greenPin, int bluePin, int delay, void (*OnActionCallBack)(), void (*OffActionCallBack)()){
    rPin = redPin;
    gPin = greenPin;
    bPin = bluePin;

    delayVal = delay;

    OnAction = OnActionCallBack;
    OffAction = OffActionCallBack;
}

void RGBHandler::Update(unsigned long currentMills)
{
    if ((currentMills - lastUpdateTime) > delayVal)
    {
        lastUpdateTime = currentMills;

        int readingR = analogRead(rPin);
        int readingG = analogRead(gPin);
        int readingB = analogRead(bPin);

        buttonStateR = readingR;
        buttonStateG = readingG;
        buttonStateB = readingB;
        

        OnAction();
        // OffAction();
    }
}