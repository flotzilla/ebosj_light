#ifndef RGB_HANDLER_H
#define TGB_HANDLER_H

#include <Arduino.h>

class RGBHandler{

public:
    int rPin;
    int gPin;
    int bPin;

    unsigned int delayVal = 30;
    
    int buttonStateR;
    int buttonStateG;
    int buttonStateB;

    unsigned long lastUpdateTime;
    unsigned long prevTime;

    unsigned long lastDebounceTime;
    unsigned long prevDebounceTime;
    unsigned int debounceDelayVal = 10;


    void (*OnAction)();
    void (*OffAction)();

    RGBHandler(int redPin, int greenPin, int bluePin, int delay, void (*OnActionCallBack)(), void (*OffActionCallBack)());

    void Update(unsigned long currentMills);
};

#endif